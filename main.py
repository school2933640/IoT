import json
import socket
import carcode_s as fc
from carcode_s.servo import Servo
from carcode_s.pwm import PWM
import time
import os
import numpy as np
from enum import Enum
import math

servo = Servo(PWM("P0"), offset=0)
servo.set_angle(0)

speeds = {40:19.48,
          50:15}
turn_times = {90:0.7, 180:1.4}

def move_precise(cm_dist,speed=50):
  global speeds
  needed_time = abs(cm_dist)/speeds[speed]
  if cm_dist > 0:
    fc.forward(speed)
  else:
    fc.backward(speed)
  time.sleep(needed_time)
  fc.stop()
  print('execution complete')
def turn_precise(angle):
  global turn_times
  fc.stop()
  needed_time = turn_times[abs(angle)]
  if angle > 0:
    fc.turn_right(75)
  else:
    fc.turn_left(75)
  time.sleep(needed_time)
  fc.stop()

print()
HOST = "192.168.0.145" # IP address of your Raspberry PI
PORT = 65432          # Port to listen on (non-privileged ports are > 1023)
traveled_distance = 0
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
  try:
    s.bind((HOST, PORT))
    s.listen()
    while True:
      print('listening standby')
      client, clientInfo = s.accept()
      print('got something\n')
      try:
        data = client.recv(1024).decode().strip()
        if data != "datareq":
          try:
            distance = int(data)
            move_precise(distance)
            traveled_distance += distance
          except Exception as e:
            print(e)
          print(f'executing {data}')
        return_payload = fc.utils.pi_read()
        return_payload.update({'total_distance':traveled_distance})
        print(f'sending back: {return_payload}')
        client.sendall(bytes(json.dumps(return_payload),'utf-8'))
      except Exception as e:
        print('tripping out with:')
        print(e)
        client.close()
        s.close()
  finally:
    fc.stop()
