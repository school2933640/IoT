const { send } = require('process');

document.onkeydown = updateKey;
document.onkeyup = resetKey;

var server_port = 65432;
var server_addr = "192.168.0.145";   // the IP address of your Raspberry PI
var send_message = '';
function client(){
    
    const net = require('net');
    
    // 
    const client_obj = net.createConnection({ port: server_port, host: server_addr }, () => {
        // 'connect' listener.
        console.log('send message: ' + send_message);
        if(send_message == ''){
            send_message = 'datareq';   
        }
        console.log('connected to server!');
        // send the message
        client_obj.write(`${send_message}\r\n`);
        send_message = '';
        
    });
    
    // get the data from the server
    client_obj.on('data', (data) => {
        document.getElementById("bluetooth").innerHTML = data;
        document.getElementById("distance").innerHTML = JSON.parse(data)['total_distance']
        document.getElementById("battery").innerHTML = JSON.parse(data)['battery']
        document.getElementById("temperature").innerHTML = JSON.parse(data)['cpu_temperature']
        document.getElementById("cpu").innerHTML = JSON.parse(data)['cpu_usage']
        console.log(data.toString());
        client_obj.end();
        client_obj.destroy();
    });

    client_obj.on('end', () => {
        console.log('disconnected from server');
    });


}

// for detecting which key is been pressed w,a,s,d
function updateKey(e) {

    e = e || window.event;

    if (e.keyCode == '87') {
        // up (w)
        document.getElementById("upArrow").style.color = "green";
        send_data("87");
    }
    else if (e.keyCode == '83') {
        // down (s)
        document.getElementById("downArrow").style.color = "green";
        send_data("83");
    }
    else if (e.keyCode == '65') {
        // left (a)
        document.getElementById("leftArrow").style.color = "green";
        send_data("65");
    }
    else if (e.keyCode == '68') {
        // right (d)
        document.getElementById("rightArrow").style.color = "green";
        send_data("68");
    }
}

// reset the key to the start state 
function resetKey(e) {

    e = e || window.event;

    document.getElementById("upArrow").style.color = "grey";
    document.getElementById("downArrow").style.color = "grey";
    document.getElementById("leftArrow").style.color = "grey";
    document.getElementById("rightArrow").style.color = "grey";
}


// update data for every 50ms
function update_data(){
    send_message = document.getElementById("message").value;
    console.log('send message84:' + send_message);
    document.getElementById("message").value = '';
    
}
setInterval(function(){
    // get image from python server
    client();
}, 500);